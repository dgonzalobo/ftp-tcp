/**
 * @author dgonzalobo
 * PROGRAMADO EN UNIX
 * 
 * Practica 4 de SBC
 * Crear un cliente y un servidor que funcionen con gestion de peticiones en paralelo
 * y que realicen una funcionalidad FTP. 
 * 
 * MEJORA 1: 
 * 	Se ofrece la posibilidad de acceder a la funcionalidad del programa atrav�s de un Menu, en lugar
 * 	de invocar por argumentos la funcionalidad deseada. 
 * 
 * MEJORA 2:
 * 	Se comprueba que el archivo existe antes de proceder a su subida al servidor, en el caso de que no exista
 * 	se pedir� por teclado el nombre de un archivo que si exista.
 * 
 * MEJORA 3:
 * 	La funci�n listarDirectorio del servidor es recursiva, por lo que se enviar� la info relativa al directorio 
 *  y subdirectorios donde se encuentre alojado el FTP.
 * 
 * MEJORA 4:
 * 	Ante la posibilidad de insertar un path para acceder al archivo de un directorio, se incorpora la opci�n de que 
 * 	si el archivo es invocado con un path, que contenga /, se procede a separarlo de dicho caracter para evitar 
 * 	problemas en la creacion de un objeto FILE.
 * 
 * MEJORA 5:
 *  Cuando sube un fichero por binario, espera a que el servidor compare que el tama�o del archivo recibido es igual al tama�o del 
 *  archivo que se le indico inicialmente, de ser as� devuelve CORRECT y se muestra por pantalla que todo ha ido bien, en 
 *  caso de no coincidir se recibe FAIL y se vuelve a intentar una enviar una vez m�s. 
 *  Se hace lo mismo en cuanto a la descarga, pero esto ya dentro del cliente. 
 *  
 * MEJORA 6:
 * 	Se implementa la opci�n de realizar la transferencia de archivos tanto de descarga como de subida, a trav�s de un 
 * 	flujo con formato texto. 
 * 
 * MEJORA 7:
 * 	Se implementa el cliente de tal forma que no e cierre al finalizar una petici�n, si no, que vuelva al menu y que de la 
 * 	opci�n de realizar una nueva petici�n o cerrar el programa. 
 * 
 * MEJORA 8;
 * 	Se implementa un sistema de autenticaci�n a trav�s de "cookies" o un parametro que se asigna aleatoriamente al cliente una vez
 * 	que se autentica en el servidor en su primera conexi�n a modo de token. De tal forma, el cliente solo necesitar� autenticarse 
 *  segun arranca el programa, y trabajar libremente, haciendo infinitas peticiones al servidor de forma segura.
 */

package es.dgonzalobo;

import java.io.*;
import java.net.*;

public class ClienteFTP_TCP {
	private DataInputStream inBytes;
	private String host;
	private BufferedReader inText;
	private DataOutputStream outBytes;
	private Socket socket;
	private PrintWriter outText;	
	private static int PUERTO;
	private static  ClienteFTP_TCP cliente;
	private BufferedReader br;
	private int accion, n, cookie;


	/**
	 * @funcion Constructor. Se le asigna un PUERTO por defecto y se abre el flujo de lectura por teclado.
	 */
	public ClienteFTP_TCP(){
		try{
			PUERTO=22569;
			br = new BufferedReader(new InputStreamReader(System.in)); 
			n=0;
			cookie=-1;
		}
		catch(Exception e){
			System.err.println("El servidor no esta conectado.");
		}		
	}

	/**
	 * @param args[0] host al que conectarse
	 * @param args[1] comando de la accion que debe realizar el cliente
	 * @param args[2] nombre del archivo.
	 * @funcion se crea un objeto cliente, si se han introducido argumentos se respetan y se llevar� a 
	 * 			cabo lo que indiquen, en caso contrario se asignar� "localhost" como host de conexi�n
	 * 			y se pasar� al run del programa.
	 */
	public static void main(String[] args){
		try{
			cliente=new ClienteFTP_TCP();
			if(args.length==0){
				cliente.host = "localhost";
				cliente.accion=0;
				cliente.run();
			}
			else if (args.length > 0) { 
				cliente.host=args[0]; 
				if(args[1].equals("DIR")){
					cliente.conectar();
					cliente.directorio();
				}
				if(args[1].equals("PUT")){
					cliente.conectar();
					cliente.subirFichero(args[2]);
				}
				if(args[1].equals("GET")){
					cliente.conectar();
					cliente.descargarFichero(args[2]);	
				}
			}
		}
		catch(Exception e){}
	}

	/**
	 * @funcion Se muestra una cabecera, para idenficar al programa, y se llama al m�todo menu. 
	 */
	public void run(){
		System.out.println("--------------------------------------------------------");
		System.out.println("\t\t     CLIENTE FTP/TCP");
		System.out.println("--------------------------------------------------------\n");
		conectar();
		menu();
	}

	/**
	 * @funcion Se muestra un menu por pantalla, a fin de ofertar la funcionalidad del programa al user.
	 * 			Una vez escogida la opcion por el user, se conectar� el cliente al server y se invocar� 
	 * 			el m�todo correspondiente
	 */
	public void menu(){
		try{
			System.out.println("\nMENU");
			System.out.println("(1)..............Descargar directorio FTP");
			System.out.println("(2)..............Subir Fichero");
			System.out.println("(3)..............Descargar Fichero");
			System.out.println("(4)..............Salir");
			accion=Integer.parseInt(br.readLine());	
			if(accion!=1&&accion!=2&&accion!=3&&accion!=4){
				System.out.println("Opcion solicitada no posible, inserte nueva opcion.");
				accion=Integer.parseInt(br.readLine());	
			}
			switch(accion){
			case(1):
				conectar();
			directorio();
			break;
			case(2):
				conectar();
			subirFichero();
			break;	
			case(3):
				conectar();
			descargarFichero();
			break;
			default:
				close();
				break;
			}
		}
		catch(Exception e){
			System.err.println("Error en al Menu. "+e);
		}

	}

	/**
	 * @funcion Se env�a por flujo de texto el comando DIR al server, y el cliente se pone a escuchar por 
	 * 			el socket hasta que reciba el comando #FIN#. Una vez sale del bucle de lectura, se cierra	
	 * 			el socket y los flujos que dependen de el, pues el servidor tiene una gesti�n por peticiones.
	 */
	public void directorio(){
		try{		
			outText.println("DIR");
			String aux;
			System.out.println("\nSERVIDOR FTP");
			while(!(aux=inText.readLine()).equals("#FIN#")){			  
				System.out.println(aux);
			}  
			desconectar();
			menu();
		}
		catch(Exception e){
			System.err.println("Error en la gestion del directorio. "+e);
		}	
	}

	/**
	 * @funcion Se solicita por pantalla el nombre del archivo que se desea descargar. Se envia al server 
	 * 			a trav�s de flujo de texto el comando GET+nombre_fichero, en el caso de que exista, el 
	 * 			server contestar� con un OK y seguido del tama�o del archivo. Se confirma que se ha recibido 
	 * 			esta info, enviando un READY al server, y se invoca el m�todo del cliente descargarBytes.
	 * 			Se cierra el socket y los flujos que dependen de el, pues el servidor tiene una gesti�n por peticiones.
	 */
	public void descargarFichero(){
		try{
			System.out.println("(1)...........Descargar con formato.");
			System.out.println("(2)...........Descargar por bytes");
			accion=Integer.parseInt(br.readLine());	
			if(accion!=1&&accion!=2){
				System.out.println("Opcion solicitada no posible, inserte nueva opcion.");
				accion=Integer.parseInt(br.readLine());	
			}
			String aux;
			System.out.println("\nEscriba el nombre del archivo que desea descargar: ");
			String fileName=br.readLine();
			if(accion==1)
				outText.println("GET-Text "+fileName);
			if(accion==2)
				outText.println("GET-Byte "+fileName);
			outText.flush();
			if((aux=inText.readLine()).equals("OK")){
				long fileSize=Long.parseLong(inText.readLine());
				outText.println("READY");
				outText.flush();
				if(accion==1)
					descargarTexto(fileName);
				if(accion==2)
					descargarBytes(fileName,fileSize);
				desconectar();
				menu();
			}
			if(aux.equals("ERROR"))
				System.out.println("ERROR: '"+fileName+"' no existe, no es posible la descarga.");
		}
		catch(Exception e){
			System.err.println("Error en la descarga del fichero. "+e);
		}
	}

	/**
	 * @param fileName nombre del fichero a descargar
	 * @funcion Es una sobrecarga de descargarFichero, a fin de dar la posibilidad de interactuar a trav�s
	 *  		de los argumentos de invocacion.
	 */
	public void descargarFichero(String fileName){
		try{
			outText.println("GET-Byte "+fileName);
			outText.flush();
			String aux;
			if((aux=inText.readLine()).equals("OK")){
				long fileSize=Long.parseLong(inText.readLine());
				outText.println("READY");
				outText.flush();
				descargarBytes(fileName,fileSize);
				cliente.desconectar();
			}
			if(aux.equals("ERROR"))
				System.out.println("ERROR: '"+fileName+"' no existe, no es posible la descarga.");
		}
		catch(Exception e){
			System.err.println("Error en la descarga del fichero. "+e);
		}
	}

	/**
	 * @param fileName nombre del archivo a descargar
	 * @param fileSize tama�o del archivo a descargar
	 * @throws IOException producida por los flujos de lectura, se le pasa al m�todo 
	 * 		   descargarFichero que la gestionar� dentro de las exceptions de descarga.
	 * @funcion Comprueba que el nombre del archivo no tiene ningun /, posteriormente se abre el flujo FileWirter
	 * 			para escrbir en un archivo creado por la clase File, que le propio FileWriter invoca, y se procede
	 * 			a leer a traves de un flujo de lectura de bytes del server, y a escrbir en el archivo creado. 
	 */
	public void descargarBytes(String fileName, long fileSize) throws IOException{
		String fileNameAux=fileName;
		while(fileNameAux.indexOf("/")>-1){     
			fileNameAux = fileNameAux.substring(fileNameAux.indexOf("/")+("/").length(),fileNameAux.length());
		}
		FileWriter writer=new FileWriter("copiaCliente_"+fileNameAux);          
		for(int i=0;i<fileSize;i++){
			writer.write(inBytes.read());
		}
		writer.close();
		if(fileSize==(new File("copiaCliente_"+fileNameAux)).length()){
			System.out.println("Archivo descargado correctamente.");
			n=0;
		}
		else{
			System.out.println("El archivo esta corrupto. Se intenta de nuevo.");
			n++;
			if(n<2)
				descargarFichero(fileName);
			else
				System.out.println("ERROR EN LA DESCARGA. ARCHIVO CORRUPTO");
		}
	}

	/**
	 * @param fileName nombre del archivo a descargar.
	 * @throws IOException excption producida por los flujos de lectura, se le pasa al m�todo 
	 * 		   descargarFichero que la gestionar� dentro de las exceptions de descarga.
	 * @funcion ante la posibilidad de que se nos facilite un path, porque se quiere acceder a algun 
	 * 			subdirectorio del server, se divide la cadena, en busca del nombre del archivo.
	 * 			Una vez qeu lo tenemos creamos un flujo PrintWriter que sea capaz de escribir las cadenas 
	 * 			de texo recibidas por el flujo con formato conectado al server.
	 */
	public void descargarTexto(String fileName) throws IOException{
		String fileNameAux=fileName;
		while(fileNameAux.indexOf("/")>-1){	
			fileNameAux = fileNameAux.substring(fileNameAux.indexOf("/")+("/").length(),fileNameAux.length());
		}
		PrintWriter writer=new PrintWriter(new BufferedWriter(new FileWriter("copiaCliente_"+fileNameAux)),true);
		String linea;
		while((linea=inText.readLine())!=null){
			writer.println(linea);
		}
		writer.close();
		System.out.println("Archivo descargado.");
	}

	/**
	 * @funcion Se solicita por teclado el nombre del archivo a subir, se comprueba qeu ese archivo, existe
	 * 			y que no es un directorio. Se comprueba que el nombre del archivo no contenga / y se envia
	 * 			al server el comando PUT+nombre_archivo seguido del tama�o del archivo. 
	 * 		    Se espera el comando READY del server, y se procede a leer el archivo a traves de subirBytes.
	 * 			Se cierra el socket y los flujos que dependen de el, pues el servidor tiene una gesti�n por peticiones.
	 */
	public void subirFichero(){ 
		try{
			System.out.println("(1)...........Subir con formato.");
			System.out.println("(2)...........Subir por bytes");
			accion=Integer.parseInt(br.readLine());	
			if(accion!=1&&accion!=2){
				System.out.println("Opcion solicitada no posible, inserte nueva opcion.");
				accion=Integer.parseInt(br.readLine());	
			}
			System.out.println("\nEscriba el nombre del fichero que desea subir. ");
			String fileName=br.readLine();
			while(!(new File(fileName)).isFile()){
				System.out.println("Archivo no encontrado, seleccione otro: ");
				fileName=br.readLine();
			}
			long fileSize=(new File(fileName)).length();
			String fileNameAux=fileName;
			while(fileNameAux.indexOf("/")>-1){	
				fileNameAux = fileNameAux.substring(fileNameAux.indexOf("/")+("/").length(),fileNameAux.length());
			}
			if(accion==1)
				outText.println("PUT-Text "+fileNameAux);
			if(accion==2){
				outText.println("PUT-Byte "+fileNameAux);
				outText.println(Long.toString(fileSize));
			}
			outText.flush();
			if((inText.readLine()).equals("READY")){
				if(accion==1){
					subirTexto(fileName);
					System.out.println("Archivo subido con exito.");
				}
				if (accion==2){
					subirBytes(fileName,fileSize);
					if(inText.readLine().equals("CORRECT")){
						System.out.println("Archivo subido con exito.");
						n=0;
					}
					else{
						System.out.println("El archivo esta corrupto. Se intenta de nuevo.");
						n++;
						if(n<2)
							subirFichero(fileName);
						else
							System.out.println("ERROR EN LA SUBIDA. ARCHIVO CORRUPTO");

					}
				}
			}
			desconectar();
			menu();
		}
		catch(Exception e){
			System.out.println("Error en la subida del fichero. "+e);
		}
	}

	/**
	 * @param fileName nombre del archivo a subir
	 * @funcion Es una sobrecarga de subirFichero, a fin de dar la posibilidad de interactuar a trav�s
	 *  		de los argumentos de invocacion.
	 */
	public void subirFichero(String fileName){ 
		try{
			while(!(new File(fileName)).isFile()){
				System.out.println("Archivo no encontrado, seleccione otro: ");
				fileName=br.readLine();
			}
			long fileSize=(new File(fileName)).length();
			String fileNameAux=fileName;
			while(fileNameAux.indexOf("/")>-1){	
				fileNameAux = fileNameAux.substring(fileNameAux.indexOf("/")+("/").length(),fileNameAux.length());
			}
			outText.println("PUT-Byte"+fileNameAux);
			outText.println(Long.toString(fileSize));
			outText.flush();
			if((inText.readLine()).equals("READY"))
				subirBytes(fileName,fileSize);
			if(inText.readLine().equals("CORRECT")){
				System.out.println("Archivo subido con exito.");
				n=0;
			}
			else{
				System.out.println("El archivo esta corrupto. Se intenta de nuevo.");
				n++;
				if(n<2)
					subirFichero(fileName);
				else
					System.out.println("ERROR EN LA SUBIDA. ARCHIVO CORRUPTO");
			}
			cliente.desconectar();
		}
		catch(Exception e){
			System.out.println("Error en la subida del fichero. "+e);
		}
	}

	/**
	 * @param fileName nombre del archivo a subir.
	 * @throws Exception producida por los flujos de lectura, se le pasa al m�todo 
	 * 		   descargarFichero que la gestionar� dentro de las exceptions de descarga.
	 * @funcion crea un flujo BufferedReader qeu leer� el archivo que hemos solicitado, para luego enviarlo
	 * 			a trav�s de un flujo con formato al server. 
	 */
	public void subirTexto(String fileName) throws Exception{
		BufferedReader reader=new BufferedReader(new FileReader(fileName));
		String linea;
		while((linea=reader.readLine())!=null){
			outText.println(linea);
		}
		reader.close();
	}

	/**
	 * 
	 * @param fileName nombre del archivo a descargar
	 * @param fileSize tama�o del archivo a decargar
	 * @throws Exception producida por los flujos de lectura, se le pasa al m�todo 
	 * 		   descargarFichero que la gestionar� dentro de las exceptions de descarga.
	 * @funcion envio del archivo al server, a trav�s de un flujo de bytes, en paquetes de 1024 bytes.
	 * 			Para la lectura del archivo el programa usa un flujo BufferedInputStream, que va leyendo 
	 * 			y almacenando la lectura para su envio. 
	 */
	public void subirBytes(String fileName, long fileSize) throws Exception{
		BufferedInputStream fileReader= new BufferedInputStream( new FileInputStream(fileName));		
		int leidos= 0;
		byte[] paquete= new byte [1024];
		while (leidos!=-1) {
			leidos=fileReader.read(paquete); 
			if (leidos!=-1)
				outBytes.write (paquete, 0, leidos);
		}
		fileReader.close();
	}

	/**
	 * @funcion desconecta el socket y todos los flujos dependientes de el. 
	 */
	public void desconectar() {
		try{
			inBytes.close(); 
			outBytes.close(); 
			inText.close(); 
			outText.close();
			socket.close();
		}
		catch(Exception e){
			System.out.println("Error en la desconexion. "+e);
		}
	}

	/**
	 * @funcion conecta el socket y abre todos los flujos que dependen del socket, lectura y escritura tanto con 
	 * 			texto como por bytes. 
	 * 			Se contempla una execption tipo ConnectException, que indicar�a que no se encuentra el server y 
	 * 			se gestionar�a cerrando el cliente.
	 */
	public void conectar(){
		try{
			socket =new Socket(host,PUERTO);
			inText = new BufferedReader(new InputStreamReader(socket.getInputStream())); 
			outText = new PrintWriter(socket.getOutputStream(), true);
			inBytes = new DataInputStream(socket.getInputStream()); 
			outBytes= new DataOutputStream(socket.getOutputStream()); 
			autenticar();
		}
		catch(ConnectException ce){
			System.out.println("No se localiza el servidor. ");
			System.exit(0);
		}
		catch(Exception e){
			System.err.println("Error en la inicializacion de los flujos. "+e);
		}
	}

	/**
	 * @throws IOException pasa la exception al menu. 
	 * @funcion envia la cookie, que si no ha habido conexi�n, ser� -1. 
	 * 			Si el server env�a FAIL, significar� que no est� registrado el cliente, o sea, que nunca ha
	 * 			habido autenticaci�n. Y comienza un proceso donde se solicita al user el nick y la pass 
	 * 			para su atenticaci�n en el server. 
	 *			En el caso de que se falle dos veces en la introducci�n de alguno de los parametros, se cerrar�
	 *			el programa.
	 */
	public void autenticar() throws IOException{
		outText.println(cookie);
		if((inText.readLine()).equals("FAIL")){
			System.out.println("Debe autenticarse en el Servidor FTP");
			System.out.println("\tUsuario:");
			outText.println(br.readLine());
			if((inText.readLine()).equals("ERROR")){
				System.out.println("Usuario erronea, introduzcalo de nuevo:");
				outText.println(br.readLine());
				if((inText.readLine()).equals("ERROR"))
					System.out.println("El usuario no existe, DESCONEXI�N.");
					System.exit(0);
			}
			System.out.println("\tPassword:");
			outText.println(br.readLine());
			if((inText.readLine()).equals("ERROR")){
				System.out.println("Password erronea, introduzcala de nuevo:");
				outText.println(br.readLine());
				if((inText.readLine()).equals("ERROR")){
					System.out.println("El usuario no existe, DESCONEXI�N.");
					System.exit(0);
				}
			}
			cookie=Integer.parseInt(inText.readLine());
		}
	}

	/**
	 * @throws IOException transmite la exception al menu.
	 * @funcion Incia el proceso de borrado del registro de autenticaci�n en el servidor, env�andole el comando
	 * 			-2, que significar� el borrado de la cookie que se pasar� despu�s por flujo con formato.
	 */
	public void close() throws IOException{
		socket =new Socket(host,PUERTO);
		inText = new BufferedReader(new InputStreamReader(socket.getInputStream())); 
		outText = new PrintWriter(socket.getOutputStream(), true);
		outText.println(-2);
		outText.println(cookie);
		inText.close();
		outText.close();
		socket.close();
		br.close();

	}
}
