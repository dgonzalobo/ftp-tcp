/**
 * @author dgonzalobo
 * PROGRAMADO EN UNIX
 * 
 * Practica 4 de SBC
 * Crear un cliente y un servidor que funcionen con gestion de peticiones en paralelo
 * y que realicen una funcionalidad FTP. 
 * 
 * En este caso el servidor, funcionar con un pool de esclavos, formados por 3 hilos, que competirn entre si, por 
 * la conexin con los clientes. 
 */

package es.dgonzalobo;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServidorFTP_TCPestandar {
		private static ServidorFTP_TCPestandar server;
		private static int PUERTO;

		public ServidorFTP_TCPestandar(){}

		/**
		 * @param args Puerto.
		 * @funcion si se recibe un argumento, será el puerto, si no, se le asigna uno por defecto. 
		 * 			se crea un objeto server del maestro, que se compartirá con los esclavos, hilos ServidorThread.
		 * 			Se llama al método run. 
		 * 			
		 */
		public static void  main (String[] args){
			if(args.length<0){
				PUERTO=Integer.parseInt(args[0]);
			}
			else{
				PUERTO=22569;	
			}
			server=new ServidorFTP_TCPestandar();
			server.run();
		}

		/**
		 * @funcion Se muestra una cabecera, indicando que es el servidor, y se crean los 3 hilos que formarán el 
		 * 			pool de esclavos. Se arrancan los 3 hilos.
		 */
		public void run(){
			System.out.println("\n  --------------------------------------------------------------------");
			System.out.println("  \t\t\t SERVIDOR FTP/TCP. ");
			System.out.println("  -------------------------------------------------------------------- \n");
			try{
				ServidorThreadEstandar serv1=new ServidorThreadEstandar("ThreadServer 1",PUERTO);
				ServidorThreadEstandar serv2=new ServidorThreadEstandar("ThreadServer 2",PUERTO);
				ServidorThreadEstandar serv3=new ServidorThreadEstandar("ThreadServer 3",PUERTO);

				serv1.start();
				serv2.start();
				serv3.start();
			}
			catch(Exception e){
				System.err.println("Error en la escucha del socket.(Run) "+e);
			}
		}
	}


	class ServidorThreadEstandar extends Thread{
		private DataInputStream inBytes;
		private static ServerSocket server_sockt;
		private int PUERTO;
		private BufferedReader inText;
		private DataOutputStream outBytes;
		private Socket socket;
		private PrintWriter outText;
		private String name, peticion;

		/**
		 * @param n nombre del hilo.
		 * @param puerto Puerto.
		 * @fucion Se le asigna un nombre al hijo, as como el puerto, que ser usado para arrancar el ServerSocket.
		 */
		public ServidorThreadEstandar(String n,int puerto){
			super(n);
			name=n;
			PUERTO=puerto;
			try{	
				server_sockt=new ServerSocket(PUERTO);
			}
			catch(Exception e){}

		}

		/**
		 * @funcion se incia un while true, donde se esucharan peticiones del cliente, a travs del serverSocket, 
		 *                      una vez llega una peticion, se conecta el socket y los correspodientes flujos, se muestra por pantalla
		 *                      un mensaje indicando el cliente que se ha aceptado y se invoca el mtodo readClient. 
		 *                      Se gestiona la exception NullPointerException del serverSocket, para indicaar que otro server se est 
		 *                      ejecutando en el mismo puerto o host.
		 */
		public void run(){
			try{
				while(true){
					socket = server_sockt.accept();
					conectar();
					System.out.println("Connection accepted in "+name+" from "+socket.getInetAddress()+" at port "+socket.getPort());
					readClient();
					close();
				}
			}
			catch(NullPointerException e1){
				System.err.println("Otro Servidor est ejecutandose sobre el mismo puerto.");
				return;
			}
			catch(Exception e){
				System.err.println("Error en el run del hilo. "+e);
			}
		}

		/**
		 * @funcion se muestra la peticion del cliente y el ID del cliente que la realzia, y en funcin del tipo
		 *                      de que sea, se procede a invocar el mtodo correspondiente.
		 */
		public void readClient(){
			try{
				System.out.println("\tClinete "+socket.getPort()+". PETICION: "+(peticion=inText.readLine()));
				if(peticion.equals("DIR"))
					listarDirectorio("./",0);
				if(peticion.startsWith("PUT"))
					subirFichero();
				if(peticion.startsWith("GET"))
					descargarFichero();
			}
			catch(Exception e){
				System.err.println("Error en la lectura de peticion del cliente. "+e);
			}
		}


		/**
		 * @funcion Comprueba si el archivo solicitado existe, si es as enva un mensaje de OK y el tamao del archivo, espera
		 *                      a que el cliente le mande el READY y comienza la transferencia de archivos en modo binario a travs de 
		 *                      paquetes de 1024 bytes. 
		 *                      Si no existe el fichero, se enva el comando ERROR
		 */
		public void descargarFichero(){
			try{
				if(new File(peticion.substring(4,peticion.length())).isFile()){
					System.out.println("\tClinete "+socket.getPort()+". ENCONTRADO: '"+peticion.substring(4,peticion.length())+"'");
					outText.println("OK");
					outText.println((new File(peticion.substring(4,peticion.length())).length()));
					outText.flush();
					if((inText.readLine()).equals("READY")){
						BufferedInputStream fileReader= new BufferedInputStream( new FileInputStream(peticion.substring(4,peticion.length())));         
						int leidos= 0;
						byte[] paquete= new byte [1024];
						while (leidos!=-1) {
							leidos=fileReader.read(paquete); 
							if (leidos!=-1)
								outBytes.write (paquete, 0, leidos);
						}
						fileReader.close();
					}
				}
				else{
					System.out.println("\tClinete "+socket.getPort()+". NO ENCONTRADO: '"+peticion.substring(4,peticion.length())+"'");
					outText.println("ERROR");
					outText.flush();
				}
			}
			catch(Exception e){
				System.err.println("Error en la descargar del archivo "+peticion.substring(4,peticion.length())+" "+e);
			}
		}


		/**
		 * @funcion lee a travs del flujo de entrada de texto, el tamao del archivo a recibir y enva READY al cliente. 
		 *                      Genera un flujo FileWriter que crea un archivo con el nombre recibido y que escribir en el los bytes recibidos
		 *                      a travs del flujo de lectura de bytes por el socket.
		 */
		public void subirFichero(){
			try{
				long fileSize=Long.parseLong(inText.readLine());
				outText.println("READY"); 
				outText.flush();

				FileWriter writer=new FileWriter("copiaServer_"+peticion.substring(4,peticion.length()));
				for(int i=0;i<fileSize;i++){    
					writer.write(inBytes.read());
				}
				writer.close();
				System.out.println("\tClinete "+socket.getPort()+". SUBIDO: '"+peticion.substring(4,peticion.length())+"' subido.");
			}
			catch(Exception e){
				System.err.println("Error en "+name+ " durante la peticion. "+e);
			}
		}               

		/**
		 * @funcion envia el directorio del servidor al cliente, mostrando en el mensaje envado si se trata de un 
		 *                      directorio o un fichero. Enva el comando #FIN# cuando acaba de envar todo el directorio.
		 *                      Se trata de una funcin recursiva, que en cuanto detecta que es un directorio, accede a el y sigue 
		 *                      envando sus ficheros, o rastrando sus directorios.
		 */
		public void listarDirectorio(String dir,int ref){
			File directorio=new File (dir);
			File contenido[]= directorio.listFiles();
			for(int i=0;i<contenido.length;i++){
				if(contenido[i].isFile()){
					for(int j=0;j<ref*4;j++)
						outText.print(" ");
					outText.println("Fichero: " + contenido[i]+ " || Peso: "+contenido[i].length());
				}
			}
			for(int i=0;i<contenido.length;i++){
				if(contenido[i].isDirectory()){
					for(int j=0;j<ref*4;j++)
						outText.print(" ");
					outText.println("DIRECTORIO: " + contenido[i]);
					listarDirectorio(contenido[i].toString(),ref+1);
				}
			}
			if(ref==0){
				outText.println("#FIN#");
			}
		}

		/**
		 * @funcion abre todos los flujos que dependen del socket, lectura y escritura tanto con 
		 *                      texto como por bytes. 
		 */
		public void conectar(){
			try{
				inBytes = new DataInputStream(socket.getInputStream()); 
				outBytes= new DataOutputStream(socket.getOutputStream()); 
				inText = new BufferedReader(new InputStreamReader(socket.getInputStream())); 
				outText = new PrintWriter(socket.getOutputStream(), true);
			}
			catch(Exception e){
				System.err.println("Error en la apertura de flujos. "+e);
			}
		}


		/**
		 * @funcion desconecta el socket y todos los flujos dependientes de el, a su vez muestra por pantalla el cliente
		 *                      que se desconecta. 
		 */
		public void close(){
			try{
				inBytes.close();
				inText.close();
				outBytes.close();
				outText.close();
				System.out.println("Disconnection confirmed in "+name +" from "+socket.getInetAddress()+" at port "+socket.getPort());
				socket.close();
			}
			catch(Exception e){
				System.err.println("Error en el cierre de los flujos. "+e);
			}
		}

	}