/**
 * @author dgonzalobo
 * PROGRAMADO EN UNIX
 * 
 * Practica 4 de SBC
 * Crear un cliente y un servidor que funcionen con gestion de peticiones en paralelo
 * y que realicen una funcionalidad FTP. 
 * 
 * MEJORA 1: 
 * 	Se ofrece la posibilidad de acceder a la funcionalidad del programa atrav�s de un Menu, en lugar
 * 	de invocar por argumentos la funcionalidad deseada. 
 * 
 * MEJORA 2:
 * 	Se comprueba que el archivo existe antes de proceder a su subida al servidor, en el caso de que no exista
 * 	se pedir� por teclado el nombre de un archivo que si exista.
 * 
 * MEJORA 3:
 * 	La funci�n listarDirectorio del servidor es recursiva, por lo que se enviar� la info relativa al directorio 
 *  y subdirectorios donde se encuentre alojado el FTP.
 * 
 * MEJORA 4:
 * 	Ante la posibilidad de insertar un path para acceder al archivo de un directorio, se incorpora la opci�n de que 
 * 	si el archivo es invocado con un path, que contenga /, se procede a separarlo de dicho caracter para evitar 
 * 	problemas en la creacion de un objeto FILE.
 * 
 * MEJORA 5:
 *  Cuando sube un fichero por binario, espera a que el servidor compare que el tama�o del archivo recibido es igual al tama�o del 
 *  archivo que se le indico inicialmente, de ser as� devuelve CORRECT y se muestra por pantalla que todo ha ido bien, en 
 *  caso de no coincidir se recibe FAIL y se vuelve a intentar una enviar una vez m�s. 
 *  Se hace lo mismo en cuanto a la descarga, pero esto ya dentro del cliente. 
 *  
 * MEJORA 6:
 * 	Se implementa la opci�n de realizar la transferencia de archivos tanto de descarga como de subida, a trav�s de un 
 * 	flujo con formato texto. 
 * 
 * MEJORA 7:
 * 	Se implementa el cliente de tal forma que no e cierre al finalizar una petici�n, si no, que vuelva al menu y que de la 
 * 	opci�n de realizar una nueva petici�n o cerrar el programa. 
 */


package es.dgonzalobo;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;


public class ServidorFTP_TCP {
	private static ServidorFTP_TCP server;
	private static int PUERTO;
	private Vector autentic;
	public String passWord, user;

	public ServidorFTP_TCP(){
		autentic=new Vector<String>();
		passWord="admin";
		user="admin";
	}

	/**
	 * @param args Puerto.
	 * @funcion si se recibe un argumento, ser� el puerto, si no, se le asigna uno por defecto. 
	 * 			se crea un objeto server del maestro, que se compartir� con los esclavos, hilos ServidorThread.
	 * 			Se llama al m�todo run. 
	 * 			
	 */
	public static void  main(String[] args){
		if(args.length<0){
			PUERTO=Integer.parseInt(args[0]);
		}
		else{
			PUERTO=22569;	
		}
		server=new ServidorFTP_TCP();
		server.run();
	}

	/**
	 * @funcion Se muestra una cabecera, indicando que es el servidor, y se crean los 3 hilos que formar�n el 
	 * 			pool de esclavos. Se arrancan los 3 hilos.
	 */
	public void run(){
		System.out.println("\n  --------------------------------------------------------------------");
		System.out.println("  \t\t\t SERVIDOR FTP/TCP. ");
		System.out.println("  -------------------------------------------------------------------- \n");
		try{
			ServidorThread serv1=new ServidorThread("ThreadServer 1",PUERTO,server);
			ServidorThread serv2=new ServidorThread("ThreadServer 2",PUERTO,server);
			ServidorThread serv3=new ServidorThread("ThreadServer 3",PUERTO,server);

			serv1.start();
			serv2.start();
			serv3.start();
		}
		catch(Exception e){
			System.err.println("Error en la escucha del socket.(Run) "+e);
		}
	}

	/**
	 * @funcion getter del Vector.
	 * @return vector de Strings 
	 */
	public synchronized Vector<String> getVector(){
		return autentic;
	}

	/**
	 * @param cookie recibe la cookie 
	 * @funcion inserta la cookie en el vector
	 */
	public synchronized void setVector(int cookie){
		autentic.add(Integer.toString(cookie));
	}
}


class ServidorThread extends Thread{
	private DataInputStream inBytes;
	private ServidorFTP_TCP server;
	private static ServerSocket server_sockt;
	private int PUERTO;
	private BufferedReader inText;
	private DataOutputStream outBytes;
	private Socket socket;
	private PrintWriter outText;
	private String name, peticion;
	private boolean flag;

	/**
	 * @param n nombre del hilo.
	 * @param puerto Puerto.
	 * @fucion Se le asigna un nombre al hijo, as� como el puerto, que ser� usado para arrancar el ServerSocket.
	 */
	public ServidorThread(String n,int puerto, ServidorFTP_TCP s){
		super(n);
		name=n;
		PUERTO=puerto;
		flag=true;
		try{
			server=s;
			server_sockt=new ServerSocket(PUERTO);
		}
		catch(Exception e){}

	}

	/**
	 * @funcion se incia un while true, donde se esucharan peticiones del cliente, a trav�s del serverSocket, 
	 * 			una vez llega una peticion, se conecta el socket y los correspodientes flujos, se muestra por pantalla
	 * 			un mensaje indicando el cliente que se ha aceptado y se invoca el m�todo readClient. 
	 * 			Se gestiona la exception NullPointerException del serverSocket, para indicaar que otro server se est� 
	 * 			ejecutando en el mismo puerto o host.
	 */
	public void run(){
		try{
			while(true){
				socket = server_sockt.accept();
				conectar();
				autenticar(inText.readLine());
				if(flag)
					readClient();
				close();
			}
		}
		catch(NullPointerException e1){
			System.err.println("Otro Servidor est� ejecutandose sobre el mismo puerto.");
			return;
		}
		catch(Exception e){
			System.err.println("Error en el run del hilo. "+e);
		}
	}

	/**
	 * @param cookie
	 * @funcion pide al cliente un usuario y una password, te permite fallar una vez si no, cerrar� la conexi�n. 
	 * 			En el caso de que se reciba un -2, se interpretar� como la orden de desconexi�n del server, por lo
	 * 			que se borrar� la cookie del cliente que solicita la desconexi�n. 
	 * 			En el caso de que la cookie ya este dentro del vector, se interpretar� como que el cliente ya se ha 
	 * 			autenticado y por tanto no se solicita pass y user.
	 */
	public void autenticar(String cookie) {
		try{
			if(Integer.parseInt(cookie)!=-2){
				flag=true;
				if(server.getVector().isEmpty()||!(server.getVector().contains(cookie))){
					flag=false;
					outText.println("FAIL");
					if(!(inText.readLine()).equals(server.user)){
						outText.println("ERROR");
						if(!(inText.readLine()).equals(server.user)){
							outText.println("ERROR");
							flag=false;
							System.out.println("Fallo en la utenticaci�n del cliente "+socket.getInetAddress());
							return;
						}
					}
					outText.println("OK");
					if(!(inText.readLine()).equals(server.passWord)){
						outText.println("ERROR");
						if(!(inText.readLine()).equals(server.passWord)){
							outText.println("ERROR");
							flag=false;
							System.out.println("Fallo en la utenticaci�n del cliente "+socket.getInetAddress());
							return;
						}
					}
					outText.println("OK");
					int c;
					server.setVector(c=(int) Math.floor(Math.random()*6+1));
					outText.println(Integer.toString(c));
					System.out.println("\tCliente "+socket.getPort()+" se ha autenticado correctamente.");
				}
				else{
					outText.println("OK");
				}
			}
			if(Integer.parseInt(cookie)==-2){
				server.getVector().remove(inText.readLine());
				System.out.println("\tBorrada cookie en el cliente "+socket.getPort());
				flag=false;
			}
		}
		catch(Exception e){
			System.err.println("El cliente se ha desconectado de repente");
		}
	}

	/**
	 * @funcion se muestra la peticion del cliente y el ID del cliente que la realzia, y en funci�n del tipo
	 * 			de que sea, se procede a invocar el m�todo correspondiente.
	 */
	public void readClient(){
		try{
			if(flag){
				System.out.println("\tCliente "+socket.getPort()+". PETICION: "+(peticion=inText.readLine()));
				if(peticion.equals("DIR"))
					listarDirectorio("./",0);
				if(peticion.startsWith("PUT-Byte"))
					subirFicheroBytes();
				if(peticion.startsWith("PUT-Text"))
					subirFicheroTexto();
				if(peticion.startsWith("GET-Byte"))
					descargarFicheroBytes();
				if(peticion.startsWith("GET-Text"))
					descargarFicheroTexto();
			}
		}
		catch(Exception e){
			System.err.println("Error en la lectura de peticion del cliente. "+e);
		}
	}

	/**
	 * @funcion Comprueba si el archivo solicitado existe, si es as� env�a un mensaje de OK y el tama�o del archivo, espera
	 * 			a que el cliente le mande el READY y comienza la transferencia de archivos en modo binario a trav�s de 
	 * 			paquetes de 1024 bytes. 
	 * 			Si no existe el fichero, se env�a el comando ERROR
	 */
	public void descargarFicheroBytes(){
		try{
			if(new File(peticion.substring(9,peticion.length())).isFile()){
				System.out.println("\tClinete "+socket.getPort()+". ENCONTRADO: '"+peticion.substring(9,peticion.length())+"'");
				outText.println("OK");
				outText.println((new File(peticion.substring(9,peticion.length())).length()));
				outText.flush();
				if((inText.readLine()).equals("READY")){
					BufferedInputStream fileReader= new BufferedInputStream( new FileInputStream(peticion.substring(9,peticion.length())));         
					int leidos= 0;
					byte[] paquete= new byte [1024];
					while (leidos!=-1) {
						leidos=fileReader.read(paquete); 
						if (leidos!=-1)
							outBytes.write (paquete, 0, leidos);
					}
					fileReader.close();	
				}
				System.out.println("\tCliente "+socket.getPort()+". DESCARGADO: '"+peticion.substring(9,peticion.length())+"' enviado.");
			}
			else{
				System.out.println("\tCliente "+socket.getPort()+". NO ENCONTRADO: '"+peticion.substring(9,peticion.length())+"'");
				outText.println("ERROR");
				outText.flush();
			}
		}
		catch(Exception e){
			System.err.println("Error en la descargar del archivo "+peticion.substring(4,peticion.length())+" "+e);
		}
	}


	/**
	 * @funcion Se comprueba si el archivo solicitado existe como fichero en la ruta que se nos indique.
	 * 			Si existe se envia un ok y el tama�o del archivo, si no, se envia un mensaje de Error.
	 * 			Si se envio el ok, se esperar� el READY del cliente y entonces a trav�s de un flujo BufferedReader
	 * 			se proceder� a leer del archivo para luego a trav�s de otro flujo con formato mandar las lineas al 
	 * 			cliente.
	 */
	public void descargarFicheroTexto(){
		try{
			if(new File(peticion.substring(9,peticion.length())).isFile()){
				System.out.println("\tClinete "+socket.getPort()+". ENCONTRADO: '"+peticion.substring(9,peticion.length())+"'");
				outText.println("OK");
				outText.println((new File(peticion.substring(4,peticion.length())).length()));
				outText.flush();
				if((inText.readLine()).equals("READY")){
					BufferedReader reader=new BufferedReader(new FileReader(peticion.substring(9,peticion.length())));
					String linea;
					while((linea=reader.readLine())!=null){
						outText.println(linea);
					}
					reader.close();
				}
				System.out.println("\tCliente "+socket.getPort()+". DESCARGADO: '"+peticion.substring(9,peticion.length())+"' enviado.");
			}
			else{
				System.out.println("\tCliente "+socket.getPort()+". NO ENCONTRADO: '"+peticion.substring(9,peticion.length())+"'");
				outText.println("ERROR");
				outText.flush();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
	}

	/**
	 * @funcion se envia el comando READY al cliente, para que comience a enviarnos las lineas del archivo, a trav�s
	 * 			de un flujo con formato. Previamente habremos creado otro flujo PrintWriter que escribir� en un archivo 
	 * 			que contar� con el nombre recibido previamente.
	 */
	public void subirFicheroTexto(){
		try{
			outText.println("READY"); 
			outText.flush();
			PrintWriter writer=new PrintWriter(new BufferedWriter(
					new FileWriter("copiaCliente_"+peticion.substring(9,peticion.length()))),true);
			String linea;
			while((linea=inText.readLine())!=null){
				writer.println(linea);
			}
			writer.close();
			System.out.println("\tCliente "+socket.getPort()+". SUBIDO: '"+peticion.substring(9,peticion.length())+"' subido.");
		}
		catch (IOException e) {
			System.out.println("Error en la recepcion del fichero de texto.");
		}
	}

	/**
	 * @funcion lee a trav�s del flujo de entrada de texto, el tama�o del archivo a recibir y env�a READY al cliente. 
	 * 			Genera un flujo FileWriter que crea un archivo con el nombre recibido y que escribir� en el los bytes recibidos
	 * 			a trav�s del flujo de lectura de bytes por el socket.
	 */
	public void subirFicheroBytes(){
		try{
			long fileSize=Long.parseLong(inText.readLine());
			outText.println("READY"); 
			outText.flush();

			FileWriter writer=new FileWriter("copiaServer_"+peticion.substring(9,peticion.length()));
			for(int i=0;i<fileSize;i++){	
				writer.write(inBytes.read());
			}
			writer.close();
			if(fileSize==(new File("copiaServer_"+peticion.substring(9,peticion.length())).length()))
				outText.println("CORRECT");
			else
				outText.println("FAIL");
			System.out.println("\tCliente "+socket.getPort()+". SUBIDO: '"+peticion.substring(9,peticion.length())+"' subido.");
		}
		catch(Exception e){
			System.err.println("Error en "+name+ " durante la peticion. "+e);
		}
	}		

	/**
	 * @funcion envia el directorio del servidor al cliente, mostrando en el mensaje env�ado si se trata de un 
	 * 			directorio o un fichero. Env�a el comando #FIN# cuando acaba de env�ar todo el directorio.
	 * 			Se trata de una funci�n recursiva, que en cuanto detecta que es un directorio, accede a el y sigue 
	 * 			env�ando sus ficheros, o rastrando sus directorios.
	 */
	public void listarDirectorio(String dir,int ref){
		File directorio=new File (dir);
		File contenido[]= directorio.listFiles();
		for(int i=0;i<contenido.length;i++){
			if(contenido[i].isFile()){
				for(int j=0;j<ref*4;j++)
					outText.print(" ");
				outText.println("Fichero: " + contenido[i]+ " || Peso: "+contenido[i].length());
			}
		}
		for(int i=0;i<contenido.length;i++){
			if(contenido[i].isDirectory()){
				for(int j=0;j<ref*4;j++)
					outText.print(" ");
				outText.println("DIRECTORIO: " + contenido[i]);
				listarDirectorio(contenido[i].toString(),ref+1);
			}
		}
		if(ref==0){
			outText.println("#FIN#");
		}
	}

	/**
	 * @funcion abre todos los flujos que dependen del socket, lectura y escritura tanto con 
	 * 			texto como por bytes. 
	 */
	public void conectar(){
		try{
			inBytes = new DataInputStream(socket.getInputStream()); 
			outBytes= new DataOutputStream(socket.getOutputStream()); 
			inText = new BufferedReader(new InputStreamReader(socket.getInputStream())); 
			outText = new PrintWriter(socket.getOutputStream(), true);
			System.out.println("Connection confirmed in "+name +" from "+socket.getInetAddress()+" at port "+socket.getPort());
		}
		catch(Exception e){
			System.err.println("Error en la apertura de flujos. "+e);
		}
	}


	/**
	 * @funcion desconecta el socket y todos los flujos dependientes de el, a su vez muestra por pantalla el cliente
	 * 			que se desconecta. 
	 */
	public void close(){
		try{
			inBytes.close();
			inText.close();
			outBytes.close();
			outText.close();
			System.out.println("Disconnection confirmed in "+name +" from "+socket.getInetAddress()+" at port "+socket.getPort());
			socket.close();
		}
		catch(Exception e){
			System.err.println("Error en el cierre de los flujos. "+e);
		}
	}

}
